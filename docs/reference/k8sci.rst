k8sci
=====

.. testsetup::

    from k8sci import *

.. automodule:: k8sci
    :members:
