========
Overview
========

A ci tool to use to deploy authenticate and deploy to k8s.

* Free software: MIT license

Installation
============

::

    pip install k8sci

You can also install the in-development version with::

    pip install https://gitlab.com/ronmallory/python-k8sci/-/archive/main/python-k8sci-main.zip


Documentation
=============


https://python-k8sci.readthedocs.io/


Development
===========

To run all the tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox
